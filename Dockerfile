FROM ubuntu:latest
MAINTAINER Mattias

ENV LANG C.UTF-8

RUN apt-get update --fix-missing && \
    apt-get -y --allow-unauthenticated upgrade && \
    apt-get -y -f --allow-unauthenticated install autoconf \
                                   build-essential \
                                   binutils-gold \
                                   bison \
                                   bzip2 \
                                   ccache \
                                   debootstrap \
                                   flex \
                                   git \
                                   libbot-basicbot-perl \
                                   libfreetype6-dev \
                                   liblua5.1-0-dev \
                                   libncurses5-dev \
                                   libncursesw5-dev \
                                   libpcre3-dev \
                                   libpng-dev \
                                   libsqlite3-dev \
                                   libsdl2-image-dev \
                                   libsdl2-mixer-dev \
                                   libsdl2-dev \
                                   libz-dev \
                                   lsof \
                                   ncurses-dev \
                                   ncurses-term \
                                   openssh-server \
                                   pkg-config \
                                   procps \
                                   python-minimal \
                                   python-pip \
                                   sudo \
                                   sqlite3 \
                                   ttf-dejavu-core \
                                   vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# To use jinja2 templates for configurations
RUN pip install j2cli pyyaml

COPY ./templates /templates
COPY ./template_vars.yml /template_vars.yml
COPY ./files /files

RUN useradd -m crawl && \
    useradd -m crawl-dev && \
    useradd -m terminal

###############
USER crawl-dev

# Don't touch these
ENV FOLDERS /crawl-master/webserver \
            /crawl-master/webserver/run \
            /crawl-master/webserver/sockets \
            /crawl-master/webserver/templates \
            /dgldir/data \
            /dgldir/dumps \
            /dgldir/morgue \
            /dgldir/rcfiles \
            /dgldir/ttyrec \
            /dgldir/data/menus \
            /dgldir/inprogress
ENV IP 0.0.0.0

RUN mkdir /home/crawl-dev/logs \
          /home/crawl-dev/run

WORKDIR /home/crawl-dev
RUN git clone -b http-timeouts-2.4 git://github.com/flodiebold/tornado.git && \
    git clone -b szorg git://github.com/neilmoore/dgamelaunch.git && \
    git clone -b szorg git://github.com/neilmoore/dgamelaunch-config.git && \
    git clone git://github.com/neilmoore/sizzell.git

WORKDIR /home/crawl-dev/tornado
RUN python setup.py build

# Temporary - won't have to wait for everything to get pulled while testing.
WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build
RUN git clone git://github.com/crawl/crawl.git crawl-git-repository

WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build/crawl-git-repository
RUN git submodule init && \
    git submodule update

###############
USER root

RUN chmod 666 /dev/ptmx
RUN echo "crawl-dev ALL=(root) NOPASSWD: /home/crawl-dev/dgamelaunch-config/bin/dgl, /sbin/install-trunk.sh, /sbin/install-stable.sh, /etc/init.d/webtiles, /sbin/remove-trunks.sh" >> /etc/sudoers

###############
USER crawl-dev

WORKDIR /home/crawl-dev/dgamelaunch/
RUN git checkout szorg && \
    ./autogen.sh --enable-debugfile --enable-sqlite --enable-shmem && \
    sed -i -e "s|-lsqlite3 -lrt|-lsqlite3 -lrt -pthread|gI" Makefile && \
    make VIRUS=1

###############
USER root

WORKDIR /home/crawl-dev/dgamelaunch/
RUN make install && \
    cp ee virus /bin

###############
USER crawl-dev

WORKDIR /home/crawl-dev/
RUN find . -type f -print0 | xargs -0 sed -i -e "s|cszo|$SERVERLIAS|gI" \
                                             -e "s|crawl.s-z.org|$SERVERURLS|gI" \
                                             -e "s|North America|$LOCATION|" \
                                             -e "s/Neil Moore (|amethyst)/$MAINTAINER/"

WORKDIR /home/crawl-dev/dgamelaunch-config
RUN sed -i -e "s|^export DGL_CHROOT=\/home\/crawl\/DGL|export DGL_CHROOT=\/|" \
           -e "s|^export DGL_UID.*$|export DGL_UID=$(id -u crawl)|" \
           -e "s|^export DGL_SERVER.*$|export DGL_SERVER=$SERVERURLS|" \
           -e "s|^export WEB_SAVEDUMP_URL=.*$|export WEB_SAVEDUMP_URL=http:\/\/$SERVERURLS|" \
           dgl-manage.conf
RUN sed -i -e "s|^CRAWL_GIT_URL=.*$|CRAWL_GIT_URL=https:\/\/github.com\/crawl\/crawl.git|" crawl-git.conf
RUN /usr/local/bin/j2 /templates/dgamelaunch.conf.j2 /template_vars.yml > dgamelaunch.conf
RUN /usr/local/bin/j2 /templates/config.py.j2 /template_vars.yml > config.py

WORKDIR /home/crawl-dev/sizzell
RUN /usr/local/bin/j2 /templates/sizzell.pl.j2 /template_vars.yml > sizzel.pl

WORKDIR /home/crawl-dev/dgamelaunch-config/crawl-build
# TODO: Run this only if we have experimentals, otherwise run the nonexperimental version (which is do nothing)
RUN sed -i -e "s|^VERS_RE=.*$|VERS_RE='^[0-9]+.[0-9]+\|${EXPERIMENTALS}$'|" update-crawl-stable-build.sh

WORKDIR /home/crawl-dev/dgamelaunch-config/utils
# TODO: Run this only if we have experimentals, otherwise run the nonexperimental version (which is a TODO)
RUN sed -i -e "s|^(.*)do_prompt 'trunk'.*|\1do_prompt 'trunk', '${VERSIONSCOMMA}', '${EXPERIMENTALSCOMMA}'|" \
           -e "s|^(.*)} elsif \(\$ver =~.*$|\1} elsif ($ver =~ /^${VERSIONS}|${EXPERIMENTALS}$/) {|" \
           trigger-rebuild.pl

WORKDIR /home/crawl-dev/dgamelanuch-config/chroot/bin
RUN /usr/local/bin/j2 /templates/init-webtiles.sh.j2 /template_vars.yml > init-webtiles.sh

WORKDIR /home/crawl-dev/dgamelanuch-config/chroot/data
RUN /usr/local/bin/j2 /templates/dgl-banner.j2 /template_vars.yml > dgl-banner

WORKDIR /home/crawl-dev/dgamelanuch-config/chroot/data/menus
RUN /usr/local/bin/j2 /templates/experimental_menu.txt.j2 /template_vars.yml > experimental.txt
RUN /usr/local/bin/j2 /templates/experimental_adv_menu.txt.j2 /template_vars.yml > experimental_adv.txt
RUN /usr/local/bin/j2 /templates/main_admin.txt.j2 /template_vars.yml > main_admin.txt
RUN /usr/local/bin/j2 /templates/main_anon.txt.j2 /template_vars.yml > main_anon.txt
RUN /usr/local/bin/j2 /templates/main_user.txt.j2 /template_vars.yml > main_user.txt
#TODO: Run script which loops menu_adv.txt and menu.txt to replace %%version%% in file with the correct version, as well as name each file with version in filename

###############
USER crawl

WORKDIR /
RUN mkdir -p crawl-master/webserver/{run|sockets|templates} && \
    mkdir -p dgldir/{dumps|morgue|rcfiles|ttyrec|data|inprogress} && \
    mkdir -p dgldir/data/menus && \
    touch dgamelaunch

# TODO: Create inprogress dir for all versions and experimentals
# TODO: Create rcfile dir for all versions and experimentals
# TODO: Create data crawl settings for all versions and experimentals
# Make sure crawl user has write access to /var/mail (0777)

###############
USER root

# TODO In the end, add an entrypoint which starts all the servers
# TODO All files that changes (data) should be mounted on a volume container or regular volume
# TODO crawl and other folders should be volumes so they can be updated
# TODO Lots of stuff, go through the below stuff and check the utils..


COPY ./utils /utils
RUN cp /home/crawl-dev/dgamelaunch/dgamelaunch /bin/
RUN chown root:root /bin/dgamelaunch
RUN chmod 755 /bin/dgamelaunch
RUN echo "/bin/dgamelaunch" >> /etc/shells
RUN chsh -s /bin/dgamelaunch terminal
RUN usermod --password `openssl passwd terminal` terminal
RUN cat /utils/sshconf.txt >> /etc/ssh/sshd_config
#RUN /utils/postbuild.sh
#CMD /utils/launch.sh

VOLUME ["/usr/games/", "/dgldir", "/crawl-master"]
EXPOSE 8080 22
